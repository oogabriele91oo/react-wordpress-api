import './App.css';
import 'bootstrap/dist/js/bootstrap';

import { Route, Routes } from 'react-router';

import Header from './components/header/Header';
import Section from './components/section/Section';
import ArticleDetail from './components/article-detail/ArticleDetail';
import Pages from './components/pages/Pages';
import Error from './components/error/Error';
import Footer from './components/footer/Footer';
import Home from './components/home/Home';


function App() {
  return (
    <div>
      <div className='cont'>
        <Header />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/categories/:id/posts" element={<Section />} />
          <Route path="/posts/:id" element={<ArticleDetail />} />
          <Route path="/pages/:id" element={<Pages />} />
          <Route path="*" element={<Error />} />
        </Routes>
        <Footer />
      </div>
    </div>
  );
}

export default App;
