export class PageModel {
    constructor(id, date, modified, title, excerpt, content, author) {
      this.id = id;
      this.date = date;
      this.modified = modified;
      this.title = title;
      this.excerpt = excerpt;
      this.content = content;
      this.author = author;
    }
  }
  
export function objectToPag(page) {
    return new PageModel(
      page.id,
      page.date,
      page.modified,
      page.title.rendered,
      page.excerpt.rendered,
      page.content.rendered,
      page.author
    );
  }
  