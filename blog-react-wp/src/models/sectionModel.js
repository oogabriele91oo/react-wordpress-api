export class SectionModel {
  constructor(id, date, slug, title, excerpt, content, author) {
    this.id = id;
    this.date = date;
    this.slug = slug;
    this.title = title;
    this.excerpt = excerpt;
    this.content = content;
    this.author = author;
  }
}

export function objectToSec(section) {
  return new SectionModel(
    section.id,
    section.date,
    section.slug,
    section.title.rendered,
    section.excerpt.rendered,
    section.content.rendered,
    section.author
  );
}
