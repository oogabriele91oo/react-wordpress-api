import { objectToArt } from "../models/articleModel";

export const getArticleId = (id) => {
    return fetch(`http://wordpress.test/wp-json/wp/v2/posts/${id}`)
        .then(response => response.json())
        .then(article => objectToArt(article))
        .catch(err => console.error(err));

};