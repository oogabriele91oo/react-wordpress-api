import { objectToSec } from "../models/sectionModel";

export const getSectionId = (id) => {
    return fetch(`http://wordpress.test/wp-json/wp/v2/posts?categories=${id}`)
        .then((res) => res.json())
        .then(sections => sections.map(section => objectToSec(section)));
};

