import { objectToPag } from "../models/pageModel";

export const getPageId = (id) => {
    return fetch(`http://wordpress.test/wp-json/wp/v2/pages/${id}`)
        .then(response => response.json())
        .then(page => objectToPag(page))
        .catch(err => console.error(err));
};