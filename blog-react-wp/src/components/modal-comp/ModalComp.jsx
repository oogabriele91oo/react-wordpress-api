import Modal from 'react-bootstrap/Modal';

export default function ModalComp(props) {
    return (
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
          <Modal.Header closeButton className="detail">
            <Modal.Title id="contained-modal-title-vcenter">
              Author
            </Modal.Title>
          </Modal.Header>
          <Modal.Body id="modal">
            <h3>Gabriele Serafini</h3>
            <p>
              In the working world since 2014 I have been training in an ecommerce company by developing commercial and relational skills, both with customers and with the work team.
              <br />
              Always fascinated by technology and software programming, I decide to train and update myself in modern web programming systems by developing skills in:
            </p>
            <ul>
              <li>HTML</li>
              <li>CSS</li>
              <li>JAVASCRIPT</li>
              <li>JQUERY</li>
              <li>PHP</li>
              <li>SQL DATABASE</li>
              <li>LARAVEL</li>
              <li>REACT.JS</li>
              <li>GIT</li>
              <li>MICROSOFT AZURE</li>
            </ul>
            <img src="/images/gabriele.jpg" alt="author" className="logoAuthor rounded-circle"/>                 
          </Modal.Body>
          <Modal.Footer className="detail">
            <button className="btn btn-outline-dark btn-lg" onClick={props.onHide}>Close</button>
          </Modal.Footer>
      </Modal>
    );
  }