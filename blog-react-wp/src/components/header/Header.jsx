import React from "react";
import { Link } from "react-router-dom";

import { objectToArt } from "../../models/articleModel.js";


export default class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cat: [],
      pages: [],
    };
  }

  readCat() {
    fetch("http://wordpress.test/wp-json/wp/v2/categories/")
      .then((res) => res.json())
      .then((categories) => {
        this.setState({ cat: categories });
      });
  }

  readPages() {
    fetch("http://wordpress.test/wp-json/wp/v2/pages")
      .then((pages) => pages.json())
      .then((pages) => {
        this.setState({ pages: pages.map((page) => objectToArt(page))
        });
      });
  }

  componentDidMount() {
    this.readCat();
    this.readPages();
  }

  render() {
    const category = this.state.cat.map((cat) => (
      <span key={cat.id}>
        <Link to={`/categories/${cat.id}/posts`} className="category"><strong>{cat.name}</strong></Link>
      </span>
    ));

    const pages = this.state.pages.map((page) => (
      <span key={page.id}>
        <Link to={`/pages/${page.id}`} className="category">{page.title}</Link>
      </span>
    ));

    const navBar = (
      <div className="container-fluid">
        <nav className="navbar navbar expand-expand-lg">
          <img src="/images/serafini.png" className="logoHome" alt="logo"></img>

          <Link to="/" > <strong>Home</strong> </Link>

          {pages}
          {category}

        </nav>        
      </div>
    );

    return (
        <div> 
          { navBar } 
        </div>
    );
  }
}