import { useParams } from "react-router-dom";
import  {useState, React} from "react";

import { getPageId } from "../../functions/pageFunction";

export default function Pages () {
    const { id } = useParams();
    const [_page , setPage] = useState({});
    

    getPageId(id)
    .then(page => setPage(page))
    
        return(
            <div className="detail">
                <h1 className="text-center display-2 txt-detail">{_page.title}</h1>
                <p className="text-end">Last Update: {_page.modified}</p>
                <p className="text-center" dangerouslySetInnerHTML= {{__html:_page.content}}></p>
                <hr />
            </div>
            
        );
     
}