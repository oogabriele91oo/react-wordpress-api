import { useState } from "react";
import { useParams } from "react-router-dom";

import { getSectionId } from "../../functions/sectionFunction";
import SectionCard from "../section-card/SectionCard";

export default function Section() {
    const [hasLoaded, setHasLoaded] = useState(false);
    const [sections, setSections] = useState([]);
    const { id } = useParams();

    if (!hasLoaded) {
        getSectionId(id).then(sections => {
            setSections(sections)
            setHasLoaded(true);
        });
    } 

    return (
        <section className="flex-parent backgroundC">
          { sections.map(section => <SectionCard key={section.id} section={section} />) }      
        </section>
    );
}
