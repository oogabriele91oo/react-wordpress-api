import React from "react";
import ArticleCom from "../article-comp/ArticleCom";

export default class Home extends React.Component {
  render() {  
    return (
      <div className="home">
        <ArticleCom />
      </div>
    );
  }
}
