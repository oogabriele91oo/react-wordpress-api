import { Link } from "react-router-dom";

export default function SectionCard(props) {
    return(
        <div className ="card-category">
            <div className="title" dangerouslySetInnerHTML= {{ __html:props.section.title }}></div>
            <hr />
            <div className="" dangerouslySetInnerHTML= {{__html:props.section.content }}></div>
            
            <p>{props.section.date}</p>

            <button type="button" className="button-category">
                <Link to={`/posts/${props.section.id}`}>Read More...</Link>
            </button>
        </div>

    );
}