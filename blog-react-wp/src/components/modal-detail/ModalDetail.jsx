import React from "react";
import ModalComp from "../modal-comp/ModalComp";


export default function ModalDetail() {
    const [modalShow, setModalShow] = React.useState(false);
  
    return (
      <>
        <button className="btn btn-outline-dark btn-lg" onClick={() => setModalShow(true)}>
          About Author
        </button>
  
        <ModalComp
          show={modalShow}
          onHide={() => setModalShow(false)}
        />
      </>
    );
}
  