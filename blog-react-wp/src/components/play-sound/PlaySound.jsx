import React, { useState } from "react";
import Sound from "react-sound";
import Beastar from "../../assets/music/beastar.mp3"


const PlaySound = (
    handleSongLoading,
    handelSongPlaying,
    handleSongFinishedPlaying
) => {
    const [isPlaying, setIsPlaying] = useState(false);

    return (
        <>
            <button className="btn btn-outline-dark btn-lg" onClick={() => setIsPlaying(!isPlaying)}>{!isPlaying ? 'Play Music!' : 'Stop' }</button>
            <Sound 
                url={Beastar}
                playStatus={
                    isPlaying ? Sound.status.PLAYING : Sound.status.STOPPED
                }
                playFromPosition={300}
                onLoading={handleSongLoading}
                onPlaying={handelSongPlaying}
                onFinishedPlaying={handleSongFinishedPlaying}
            />
        </>
    );
};

export default PlaySound;