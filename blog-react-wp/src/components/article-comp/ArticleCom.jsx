import React from "react";
import { Link } from "react-router-dom";

import { objectToArt } from "../../models/articleModel.js";

export default class ArticleCom extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      articles: [],
    };
  }

  componentDidMount() {
    fetch("http://wordpress.test/wp-json/wp/v2/posts")
      .then((res) => res.json())
      .then((articles) => {
        this.setState({
          articles: articles.map((article) => objectToArt(article)),
        });
      });
  }

  render() {
    const card = this.state.articles.map((article) => (
      <div className="cards " key={article.id}>
        <div className="title"> 
            {article.title} 
        </div>
        <div
          className="preview"
          dangerouslySetInnerHTML={{ __html: article.excerpt }}
        ></div>

        <button type="button" className="button">
          <Link to={`/posts/${article.id}`}>Read More...</Link>
        </button>
      </div>
    ));

    return ( 
        <div className="flex-parent backgroundC"> 
            {card} 
        </div> 
    );
  }
}
