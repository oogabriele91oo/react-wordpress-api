import React from "react";
import { Link } from "react-router-dom";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebookF,
  faInstagram,
  faLinkedinIn,
  faTwitter,
} from "@fortawesome/free-brands-svg-icons";

import PlaySound from "../play-sound/PlaySound";
import ModalDetail from "../modal-detail/ModalDetail";


export default class Footer extends React.Component {
  render() {
    const fb = <FontAwesomeIcon icon={faFacebookF} />;

    const ig = <FontAwesomeIcon icon={faInstagram} />;
    
    const linkedin = <FontAwesomeIcon icon={faLinkedinIn} />;
    
    const twitter = <FontAwesomeIcon icon={faTwitter} />;
    
    return (
      <footer className="container-fluid">
        <address className="flex-parent">
          <div>
            <img src="/images/serafini.png" alt="logo" className="logo"></img>
            
          </div>
          <div className="category">
            <div>Resources</div>
            <div>Supports</div>
            <div>Services</div>
          </div>
          <div className="category">
            <div><Link to={"/pages/17"}>About us</Link></div>
            <div><Link to={"/pages/15"}>Contact us</Link></div>
            <div><Link to={"/pages/3"}>Privacy Policy</Link></div>
          </div>
          
          <div className="social">
            <span>
              <a href="https://facebook.com">{fb}</a>
            </span>
            <span>
              <a href="https://instagram.com">{ig}</a>
            </span>
            <span>
              <a href="https://linkedin.com/in/">{linkedin}</a>
            </span>
            <span>
              <a href="https://twitter.com">{twitter}</a>
            </span>
          </div>
        </address>
        <div className="row">
          <ModalDetail />
          <PlaySound />
        </div>

        <div className="copyright">&#169;copyright. All rights reserved.</div>
      </footer>
    );
  }
}
