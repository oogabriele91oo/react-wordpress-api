import React from "react";

export default class Error extends React.Component {
  render() {
    return (
      <div className="alert text-center" role="alert">
        <h1 className="display-1">404 not found</h1>
      </div>
    );
  }
}
