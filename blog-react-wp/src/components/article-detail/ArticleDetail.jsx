import { useParams } from "react-router-dom";
import  {useState, React} from "react";

import { getArticleId } from "../../functions/articleFunction";

export default function ArticleDetail () {
    const { id } = useParams();
    const [_article , setArticle] = useState({});
    

    getArticleId(id)
    .then(article => setArticle(article))
    
    return(
        <section>
            <div className="article text-center">
                <h1 className="txt-article display-1">Japan Blog</h1>
            </div>
            <div className="detail">
                <h1 className="text-center display-2 txt-detail">{_article.title}</h1>
                <span>{_article.date}</span>
                <p dangerouslySetInnerHTML= {{__html:_article.content}}></p>
                <span><em>{_article.cat}</em> </span>
            </div>
        </section>
            
    );
       
}